// Por medio de esta línea de código activamos el modo estricto.
'use strict';

/**
 * ##############################
 * ## Declaración de variables ##
 * ##############################
 *
 * La primera vez que creamos una variable en nuestro código es necesario
 * declararla mediante el uso de la palabra reservada "const" o "let".
 *
 *  - const: se usa para declarar variables constantes cuyo valor no va
 *           a ser modificado.
 *
 *  - let: se usa para declarar variables cuyo valor puede variar en un
 *         futuro.
 *
 * ¡Siempre que podamos usar "const" debemos usar "const"!
 *
 */

// Declaramos la variable edad.
let age = 20;

// Mediante "console.log" podemos mostrar el valor de una variable por consola.
console.log(age);

// Declaramos la constante color favorito.
const favoriteColor = 'black';

console.log(favoriteColor);

// Si intentamos modificar el valor de una constante obtendremos un error.
// favoriteColor = 'white';

// Si queremos modificar el valor de una variable ya declarara no debemos
// volver a declararla. Simplemente le asignamos un nuevo valor,
age = 22;

// Dado que en la línea 38 modificamos el valor de la variable edad este
// "console.log" nos indicará que "edad = 22". Podemos entender
// que el código JavaScript se ejecuta línea a línea de arriba hacia abajo
// dado que en el "console.log" de la línea 26 "edad = 20".
console.log(age);

// Podemos mostrar mensajes personalizados incluyendo el valor de una o varias
// variables concatenando cadenas de texto con variables de esta forma. Es importante
// indicar los espacios si así lo requerimos.
console.log(
    'Tengo ' + age + ' años y mi color favorito es el ' + favoriteColor + '.'
);

// Podemos ejecutar un fragmento de código u otro en función de si una condición
// se cumple o no. Para ello usaremos el bloque "if".
if (age > 17) {
    // Si la condición que hay entre paréntisis es verdadera se ejecuta todo
    // el código que haya en este bloque.
    console.log('Mayor de edad');
} else {
    // De lo contrario se ejecuta todo el código que haya en este otro bloque.
    // Que exista un "if" no implica que deba existir un "else".
    console.log('Menor de edad');
}

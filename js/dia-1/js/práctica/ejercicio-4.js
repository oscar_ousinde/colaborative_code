'use strict';

/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Escribe un programa que lea tres valores enteros los cuales significan día, mes y año
 * respectivamente. Debes comprobar si la fecha es válida. Si lo es debes imprimir un mensaje
 * indicando el mes con su nombre. Si no lo es debes imprimir un mensaje indicando que hubo
 * un error.
 *
 * Para simplificar el ejercicio se considerará que:
 *
 *  - Todos los meses tienen 31 días.
 *
 *  - Todos los años tienen 3 meses: enero, febrero y marzo.
 *
 *  - Solo serán válidos los años del 0 al 2030 (incluyendo ambos valores).
 *
 */

/* const day = 30;
const month = 1;
const year = 2010;

let myArray = [day, month, year];

if (myArray[0] <= 31) {
    switch (month) {
        case 1:
            if (year >= 0 && year <= 2030) {
                console.log(`${day} de enero del año ${year}`);
            } else {
                console.log('El año debe estar entre 0 y 2030');
            }
            break;
        case 2:
            if (year >= 0 && year <= 2030) {
                console.log(`${day} de enero del año ${year}`);
            } else {
                console.log('El año debe estar entre 0 y 2030');
            }
            break;
        case 3:
            if (year >= 0 && year <= 2030) {
                console.log(`${day} de enero del año ${year}`);
            } else {
                console.log('El año debe estar entre 0 y 2030');
            }
            break;
        default:
            console.log(
                'El mes debe ser introducido como el valor 1 (Enero), 2 (Febrero) o 3 (Marzo)'
            );
    }
} else {
    console.log('Solo se admite el valor de 0 a 31 para los días');
} */

const day = 3;
const month = 'enero';
const year = 2010;

let myArray = [day, month, year];

function yearCheck() {
    if (year >= 0 && year <= 2030) {
        console.log(`Día ${day} de ${month} del año ${year}`);
    } else {
        console.log('El año debe estar entre 0 y 2030');
    }
}

if (myArray[0] > 0 && myArray[0] <= 31) {
    switch (month) {
        case 'enero':
            yearCheck(month);
            break;
        case 'febrero':
            yearCheck(month);
            break;
        case 'marzo':
            yearCheck(month);
            break;
        default:
            console.log('Los meses válidos son enero, febrero o marzo');
    }
} else {
    console.log('Solo se admite el valor de 1 a 31 para los días');
}

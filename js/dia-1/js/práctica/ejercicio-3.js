'use strict';

/**
 * #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Escribe un programa que, dados tres valores enteros, muestre por consola
 * el mayor de ellos.
 *
 */

const num1 = 1;
const num2 = 2;
const num3 = 3;

if (num1 > num2 && num1 > num3) {
    console.log(`El mayor valor es: ${num1}`);
} else if (num2 > num3) {
    console.log(`El mayor valor es: ${num2}`);
} else {
    console.log(`El mayor valor es: ${num3}`);
}

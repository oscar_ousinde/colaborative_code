'use strict';

/**
 * #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Escribe un programa que, dados dos valores enteros, muestre por consola
 * la división del mayor entre el menor.
 *
 */

// Declaramos el primer número.
let numA = 4;

// Declaramos el segundo número.
let numB = 'a';

// Comprobamos si el valor A es mayor que B.
if (numA > numB) {
    // Si el valor A es mayor que B mostramos por consola la
    // división de A entre B.
    console.log(numA / numB);
} else {
    // Si el valor A NO es mayor que B mostramos por consola la
    // división de B entre A.
    console.log(numB / numA);
}

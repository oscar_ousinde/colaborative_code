'use strict';

/**
 * #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Escribe un programa que, dada una edad y un peso, muestre por consola
 * un mensaje del tipo: "Tienes 36 años y pesas 45kg."
 *
 */

// Declaramos la edad.
let age = 18;

// Declaramos el peso.
let weight = 70;

// Mostramos un mensaje personalizado con la ayuda de las dos
// variables anteriormente declaradas. Es importante dejar los
// espacios en blanco donde consideremos necesario.
console.log('Tienes ' + age + ' años y pesas ' + weight + 'kg.');

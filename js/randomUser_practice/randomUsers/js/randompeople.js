/**
 * ##################
 * ## Random Users ##
 * ##################
 *
 * Crea una función que reciba como argumento un número entero que representará
 * un número de usuarios. Dentro de esa función realiza una llamada al
 * API "https://randomuser.me/api" y agrégale el query string "?results=10" donde
 * "10" hace referencia al parámetro que indica el número de usuarios, es decir, no
 * hay que poner exactamente "10".
 *
 * En el body de la respuesta obtendrás un array con un listado de usuarios.
 * Crea un <li> por usuario y agrégalo al <ul> con clase "userlist".
 *
 * El interior de cada <li> debería tener esta estructura:
 *
 *      <article>
 *           <header>
 *               <img src="https://www.example.com" alt="Sofía González">
 *               <h1>Sofía González</h1>
 *           </header>
 *           <p>A Coruña (España), 1981</p>
 *      </article>
 *
 * Llama a la función y envíale como argumento un número de usuarios. Comprueba
 * que se muestra todo correctamente.
 *
 */

'use strict';

//seleccionar ul del html
const charList = document.querySelector('ul.userlist');


//Función asincrona
const getUsers = async (userNum) => {
    try {
        //Vaciar ul
        charList.innerHTML = '';

        //Peticion al servidor
        const response = await fetch(`https://randomuser.me/api?results=${userNum}`);

        //Obtener el body
        const body = await response.json();
        
        //hacemos el fragment
        const frag = document.createDocumentFragment();

        //recorrer bucle
        for (const person of body.results) {
            //Crear li
            const li = document.createElement('li');

            //Agregamos contenido al li
            li.innerHTML = `
              <article>
                   <header>
                       <img src="${person.picture.large}" alt="${person.name}">
                       <h2>${person.name.title} ${person.name.first} ${person.name.last}</h2>
                   </header>
                   <p>${person.location.city} (${person.location.country}), ${person.dob.date.slice(0,4)}</p>
              </article>
            `

            //Pushear el contenido del li al fragment
            frag.append(li);
        }

        //pushear el frag al html
        charList.append(frag);

    } catch (err) {
        console.error('err');
    }
}



getUsers(15);